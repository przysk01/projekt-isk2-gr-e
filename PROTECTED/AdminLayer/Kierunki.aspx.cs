﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PROTECTED_AdminLayer_Kierunki : System.Web.UI.Page
{
    private static string path = HttpContext.Current.Server.MapPath("~/App_Data/DziekanatDB.mdf");
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Kierunek();
        }
    }
    protected void Kierunek()
    {
        DziekanatDBContextDataContext db = new DziekanatDBContextDataContext();
        var kierunki = db.Kierunkis.ToList();
        GridView1.DataSource = kierunki;
        GridView1.DataBind();
        
    }

    protected void GridView1_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        //kasowanie po id, musialem dodać do Gridview DataKeyNames="Id"
        int id = Int32.Parse(GridView1.DataKeys[e.RowIndex]["Id"].ToString());
        using(DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            var kierunek = (from k in db.Kierunkis where k.Id == id select k).FirstOrDefault();
            db.Kierunkis.DeleteOnSubmit(kierunek);
            db.SubmitChanges();
        }
        Kierunek();
    }

    protected void SzukajKierunek()
    {
       using(DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
       {
           var kierunki = from kierunek in db.Kierunkis where kierunek.nazwa == TextBox1.Text select kierunek;
           if (kierunki.Count() == 0)
           {
               Label1.Text = "<b>Brak podanego kierunku</b>";
           }
           else
           {
               Label1.Text = "";
               GridView1.DataSource = kierunki;
               GridView1.DataBind();
           }
       }
       
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        SzukajKierunek();
        TextBox1.Text = "";
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        string nazwa = TextBox1.Text;
        //DodajKierunek(nazwa);
        Kierunek();
        TextBox1.Text = "";
        Response.Write("<script>alert('Dodano kierunek " + nazwa + "');</script>");
    }

    protected void WyszukajStudentowPoKierunku(int id)
    {
        using(DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            var studenci = from student in db.Studencis where student.idkierunku == id select student;
            GridView2.DataSource = studenci;
            GridView2.DataBind();
        }
    }

    protected void WyszukajPRzedmiotyPoKierunku(int id)
    {
        using(DziekanatDBContextDataContext db = new DziekanatDBContextDataContext())
        {
            var diffprzedmioty = (from przedmiotwykl in db.PrzedmiotWykladowcas where przedmiotwykl.idkierunku == id select przedmiotwykl).GroupBy(p => p.idprzedmiotu).Select(grpb => grpb.First());
            var przedmioty = from przedmiot in diffprzedmioty select przedmiot.Przedmioty;
            GridView2.DataSource = przedmioty;
            GridView2.DataBind();
        }
    }
    //protected void dodajkierunek(string nazwa)
    //{
    //    try
    //    {
    //        if (nazwa.length == 0)
    //        {
    //            throw new exception();
    //        }
    //        using(dziekanatdbcontextdatacontext db = new dziekanatdbcontextdatacontext())
    //        {
    //            kierunek kierunek = new global::kierunek();
    //            kierunek.nazwa = nazwa;
    //            db.kierunkis.insertonsubmit(kierunek);
    //            db.submitchanges();
    //        }
    //    }
    //    catch (exception e)
    //    {

    //         response.write("<script>alert('błąd dodawania rekordu');</script>");
    //    }

    //}
    protected void Button3_Click(object sender, EventArgs e)
    {
        var tekst = TextBox2.Text;
         try
        {
            if (tekst.Length == 0)
            {
                throw new Exception();
            }
            WyszukajStudentowPoKierunku(Int32.Parse(TextBox2.Text));
            TextBox2.Text = "";
        }
        catch (Exception ex)
        {

             Response.Write("<script>alert('błąd wyszukiwania. Spróbuj ponownie');</script>");
      }
       
    }
    protected void Button4_Click(object sender, EventArgs e)
    {
        WyszukajPRzedmiotyPoKierunku(Int32.Parse(TextBox2.Text));
        TextBox2.Text = "";
    }
    protected void Button5_Click(object sender, EventArgs e)
    {
        Kierunek();
    }
    
    //          **********************************************************
    protected void btnNewEntry_Click(object sender, EventArgs e)
    {
        Page.ClientScript.RegisterStartupScript(
        this.GetType(), "OpenWindow", "window.open('NewSubject.aspx','_newtab');", true);
    }





}